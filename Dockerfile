FROM docker.io/ubuntu:latest

ENV version=1.2.4
# Installing runtime deps (java-runtime).
RUN apt update && apt install default-jre fish openssl -y

# Installing building deps.
RUN apt install wget -y
RUN mkdir /app && cd /app && wget 'https://github.com/crpmax/mc-bots/releases/download/v'$version'/mc-bots-'$version'.jar'

ADD ./start.sh /bin/start

CMD fish
