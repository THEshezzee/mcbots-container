#!/bin/bash 
# Configuration
count=150 # Value of max bots.
proxy_type=SOCKS4 # SOCKS5 or SOCKS4.
register_command="/register 1234512 1234512"
prefix=$(openssl rand -hex 2) # Bot names prefix. (For e.g "Bot" or "$(openssl rand -hex 2)" without quotes).
proxy_url="https://github.com/monosans/proxy-list/raw/main/proxies/socks4.txt" 

java -jar /app/* -d 1 10 -r -c $count  -t $proxy_type -l $proxy_url -j $register_command -p $prefix -s $1
