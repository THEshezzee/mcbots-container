#!/bin/bash
container_ctl=docker # Docker or podman.
tag="registry.gitlab.com/theshezzee/mcbots-container:1.19"

$container_ctl build -f Dockerfile -t $tag .
